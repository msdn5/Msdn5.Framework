﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Text;
using System.Threading.Tasks;


namespace Msdn5.Framework
{
    /// <summary>
    /// 机器码相关
    /// </summary>
    public class MachineCode
    {
        /// <summary>
        /// 取当前机器名称(设备名称)
        /// </summary>
        /// <returns>机器名称(设备名称)</returns>
        public static string GetHostName()
        {
            return System.Net.Dns.GetHostName();
        }
        /// <summary>
        /// 获取机器CPUID
        /// *会重复
        /// </summary>
        /// <returns>CPUID</returns>
        public static string GetCpuInfo()
        {
            string cpuInfo = "";
            try
            {
                using (ManagementClass cimobject = new ManagementClass("Win32_Processor"))
                {
                    ManagementObjectCollection moc = cimobject.GetInstances();
                    foreach (ManagementObject mo in moc)
                    {
                        cpuInfo = mo.Properties["ProcessorId"].Value.ToString();
                        mo.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return cpuInfo;
        }
        /// <summary>
        /// 获取硬盘信息
        /// 硬盘信息[硬盘序列号]
        /// 多块硬盘时按照|分隔每块的名称
        /// </summary>
        /// <returns>硬盘型号</returns>
        public static string GetHardDiskInfo()
        {
            List<string> hdlist = new List<string>();
            try
            {
                using (ManagementClass cimobject = new ManagementClass("Win32_DiskDrive"))
                {
                    ManagementObjectCollection moc = cimobject.GetInstances();
                    foreach (ManagementObject mo in moc)
                    {
                        string info = "";
                        var model = mo.Properties["model"].Value;//磁盘驱动器的制造商的型号
                        if (model != null)
                        {
                            info = $"{model.ToString().Trim()}";
                        }
                        var serialNum = mo.Properties["SerialNumber"].Value;//序列号
                        if (serialNum != null)
                        {
                            info += $"[{serialNum.ToString().Trim()}]";
                        }
                        if (info.NotNullOrEmpty())
                        {
                            hdlist.Add(info);
                        }
                        mo.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Join("|", hdlist);
        }
        /// <summary>
        /// 获取硬盘序列号
        /// 多块硬盘时按照|分隔每块的序列号
        /// </summary>
        /// <returns>硬盘序列号</returns>
        public static string GetHardDiskSerialNumber()
        {
            List<string> hdlist = new List<string>();
            try
            {
                ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT * FROM Win32_PhysicalMedia");
                foreach (ManagementObject mo in searcher.Get())
                {
                    //序列号
                    hdlist.Add(mo["SerialNumber"]?.ToString().Trim());
                    mo.Dispose();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Join("|", hdlist);
        }
        /// <summary>
        /// 获取网卡Mac
        /// 多块网卡时按照|分隔每块的名称
        /// </summary>
        /// <returns>网卡Mac</returns>
        public static string GetMacAddress()
        {
            List<string> hdlist = new List<string>();
            try
            {
                using (ManagementClass mc = new ManagementClass("Win32_NetworkAdapterConfiguration"))
                {
                    ManagementObjectCollection moc2 = mc.GetInstances();
                    foreach (ManagementObject mo in moc2)
                    {
                        if ((bool)mo["IPEnabled"] == true)
                            hdlist.Add(mo["MacAddress"].ToString());
                        mo.Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return string.Join("|", hdlist);
        }
    }
}
