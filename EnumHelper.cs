﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    /// <summary>
    /// 枚举类的一些帮助文件
    /// </summary>
    public static class EnumHelper
    {
        /// <summary>
        /// 获取枚举类型的Name
        /// </summary> 
        /// <param name="En">枚举</param>
        /// <returns>当前的Name</returns>
        public static string GetName(this Enum En)
        {
            
            return Enum.GetName(En.GetType(), En);
        }

    }
}
