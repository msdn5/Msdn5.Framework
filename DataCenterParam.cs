﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    public class DataCenterParam
    {
        public string Action { get; set; }

        public string Data { get; set; }

        public T GetData<T>()
        {
            try
            {
                return Data.UrlDecoding().ToBase64Bytes().ToStr(new ParsingOption()
                {
                    ParsingMode = ParsingModeEnum.Encoding
                }).UrlDecoding().JsonTo<T>(); 
            }
            catch
            {
                return default(T);
            }

        }
    }
}
