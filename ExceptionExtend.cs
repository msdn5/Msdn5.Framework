﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    /// <summary>
    /// 针对异常的扩展
    /// </summary>
    public static class ExceptionExtend
    {
        /// <summary>
        /// 获取Message,如果InnerException不为Null也获取
        /// </summary>
        /// <param name="exp">异常信息</param>
        /// <returns>完整的异常信息</returns>
        public static string GetErrorMessage(this Exception exp)
        {
            StringBuilder builder = new StringBuilder();
            builder.AppendLine(exp.Message);
            if (exp.InnerException != null)
            {
                builder.AppendLine(exp.InnerException.GetErrorMessage());
            }
            return builder.ToString();
        }
    }
}
