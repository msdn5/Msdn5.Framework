# Msdn5.Framework

#### 介绍
涵盖时间处理、字符串转换、JSON处理、表达式树、常用加解密、文件读取写入等等常用扩展库,使开发更便捷,不用编写太多重复代码

#### 项目最低环境
NetFramework 4.5 与 NetCore3.1


#### 安装教程

1.  拉取回代码直接引用工程即可

#### 使用说明

除极个别外需要创建实体,包括但不限于(DatetimeEx、CipherOption、ExpressionComparer、FileExtend)
其他全是封装的扩展方法.

```
#ConfigHelper (配置/文件/读取写入)
Fx4.5下依赖System.Management读 *.appconfig文件中的 ConnectionString与AppSetting并且可将当前结果转Int与Bool
并且实现读写文件(返回byte[]与字符串)

#CustomExpression (自定义表达式) web*
可动态拼接并且生成表达式

#DataApiParam (按照固定格式搜索数据,配合CustomExpression) web*

#DataCenterParam (数据传输的基本格式) web*

#RepMsg (通用回复消息) web*

#QueryItem 后续将移除 web*
搜索时的类,兼容老程序,新的已拆分为  DataApiParam DataCentreParam

web* 在web时常用


#DateTimeExHelper(关于时间的扩展)
 string str = DateTime.Now.ToStr(); // 获取当前时间的标准字符串(带标准符号 秒级) 格式 yyyy-MM-dd HH:mm:ss
 
 var plist = DateTime.Now.GetPastDays(7); //   获取过去几日的集合(n天前至今所有的日期) 注意,集合包含今日.(最后一天为今天) 只取日期部分 GetPastDays().Select(v=>v.StartTimeDate).ToArray()

 var flist = DateTime.Now.GetFutureDays(7); // 获取未来几日的集合(今天至未来间所有的日期)注意,默认时间包含今日.(第一天为今天) 

 DateTime dt = "2020年01月03日".ToDateTime(); //一个字符串时间转换成DateTime对象
 DateTime dt1 = "2020-01-03".ToDateTime();

 DatetimeEx dtx = "2023-12-26".ToDateTimeEx(); // 一个字符串时间转换成DateTimeEx对象DateTimeEx 包含开始时间 例:2023-12-26 00:00:00,结束时间2023-12-26 23:59:59
 //当然可以传递一个DateTime/字符串/也可以指定开始时间与结束时间

 string sp = DateTime.Now.ToTimeStamp10(); //时间戳相互转换
 string sp13 = DateTime.Now.ToTimeStamp13();
 DateTime dt2 = sp13.TimeStampToDateTime();

#EnumHelper (获取一个枚举的所有Name)

#ExceptionExtend (获取异常的Message含InnerException)

#ExpressionComparer (表达式比较,继承IEqualityComparer)
//可以比较两个类/类中的某个属性(调用自身比较)
new ExpressionComparer <实体类型, 实体类型或object>(x =>x.属性或字段))
例:
class Test{
	public int Id {get;set;}
	public string Name {get;set;}
}
List<Test> tlist = new List<Test>();
tlist.Add(new Test(){Id=1,Name="t2"});
tlist.Add(new Test(){Id=2,Name="t2"});
tlist.Distinct<Test>(new ExpressionComparer <Test,string>(x =>x.Name)));//比较Test类中的Name属性,不仅仅在去重使用,任何比较的地方都可以调用
 

#FileExtend  一个File扩展类(只读的形式打开文件,不锁定文件)

#GlobalReferences 提供一些全局引用(其他库依赖)

#JsonHelper (依赖Newtonsoft.Json 提供转换)
var json="{user = 'admin',pass='123456'}".JsonTo();//Json文本直接转JObject;另包含JsonTo<T>
var obj = new {user="admin",pass="123456"}.ToJson();也可以任意对象转json文本.

#MachineCode 获取机器相关信息(CPU/硬盘/内存/)

#NumHelper 数字的一些常用封装(随机生成double decimal)

#ObjectHelper 
"111".Convert<double>();
"True".Convert<bool>();
还有一些反射操作,如读写某个类的某个属性



#RandomString 生成随机字符/随机数值,随机数值数组/汉字/生僻字


#StringHelper  一堆String类型的扩展方法
EqualsIgnoreCase //字符串比较不区分大小写
Split/SplitEx 常用封装不用编写StringSplitOptions
IsNullOrEmpty 系统 IsNullOrWhiteSpace 的结果 判断是否为空字符串
NotNullOrEmpty 判断不为空,对IsNullOrEmpty取反
OpenUrl 打开一个URL
OpenDir 打开一个文件夹
Join<T> 任意数组,组合成字符串
HTML编码/解码
URL编码/解码
Base64编码/解码
Unicode编码/解码
向左/向右填充数据
各种字符串转Byte数组/数组反转字符串
"一些字符串".UrlEncoding(); //对字符串进行URL编码
"一些字符串".ToBytes([hex,base64,encoding]);//转换成字节数组

```
