﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    //放一些其他依赖库的引用



    /// <summary>
    /// 解析参数
    /// </summary>
    public enum ParsingModeEnum
    {
        Base64,
        Hex,
        Encoding
    };
    /// <summary>
    /// 解析选项类
    /// </summary>
    public class ParsingOption
    {
        /// <summary>
        /// 解析模式
        /// </summary>
        public ParsingModeEnum ParsingMode { get; set; } = ParsingModeEnum.Encoding;
        /// <summary>
        /// 解析的编码
        /// </summary>
        public Encoding ParsingEncoding { get; set; } = Encoding.UTF8;

        public Action<Exception> ErrorCallBack { get; set; }

    }
}
