﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    /// <summary>
    /// 搜索时的类,兼容老程序,新的已拆分为  DataApiParam DataCentreParam
    /// </summary>
    public class QueryItem<T>
    {
        public int page { get; set; } = 1;
        public int limit { get; set; } = 10;
        public string Action { get; set; }
        public T Data { get; set; }
        public List<T> Datas { get; set; }
        public string searchKey { get; set; }
        public string searchVal { get; set; }
        public string searchSelKey { get; set; }
        public string searchSelVal { get; set; }
        public string searchDateKey { get; set; }
        public string searchDateVal { get; set; }
       
        public string field { get; set; }
        public int order { get; set; }
        public DateTime StartTime { get => GetTime(0); }
        public DateTime EndTime { get => GetTime(1); }
        DateTime GetTime(int index)
        {
            DateTime dt = DateTime.Now.Date;
            if (!string.IsNullOrEmpty(searchDateVal))
            {
                DateTime dd;
                if (DateTime.TryParse(searchDateVal.Split(new string[] { " 到 " }, StringSplitOptions.RemoveEmptyEntries)[index], out dd))
                {
                    dt = dd;
                }
                else
                {
                    dt = DateTime.Now;
                }
                if (index > 0)
                {
                    dt = dt.AddDays(1).AddMinutes(-1);
                }
            }
            return dt;
        }
    }
}
