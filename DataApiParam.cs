﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    public class DataApiParam
    {
        /// <summary>
        /// 返回的数据类型 treelist
        /// </summary>
        public string ResultType { get; set; }
        #region 分页
        public int Page { get; set; } = 1;
        public int Limit { get; set; } = 10;

        #endregion

        #region 内容搜索
        /// <summary>
        /// 内容字段名
        /// </summary>
        public string SearchKey { get; set; }
        /// <summary>
        /// 内容字段值(可模糊)
        /// </summary>
        public string SearchVal { get; set; }

        #endregion

        #region 数值搜索
        /// <summary>
        /// 数值字段名
        /// </summary>
        public string SearchNumKey { get; set; }
        /// <summary>
        /// 起始值
        /// </summary>
        public string SearchNumStartVal { get; set; }
        /// <summary>
        /// 终止值
        /// </summary>
        public string SearchNumEndVal { get; set; }
        #endregion

        #region 选择搜索
        /// <summary>
        /// 选择字段名
        /// </summary>
        public string SearchSelKey { get; set; }
        /// <summary>
        /// 选择字段值
        /// </summary>
        public string SearchSelVal { get; set; }

        #endregion

        #region 日期搜索
        /// <summary>
        /// 时间字段名
        /// </summary>
        public string SearchDateSelKey { get; set; }
        /// <summary>
        /// 开始时间值
        /// </summary>
        public string SearchStartTimeVal { get; set; }
        /// <summary>
        /// 结束时间值
        /// </summary>
        public string SearchEndTimeVal { get; set; }
        #endregion

    }
}
