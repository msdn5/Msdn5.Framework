﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    /// <summary>
    /// 回复的数据格式
    /// </summary>
    public class RepMsg<T>
    {
        /// <summary>
        /// 执行结果
        /// 大于0执行成功. 
        /// 等于0执行失败
        /// 小于0执行异常.
        /// </summary>
        public int Code { get; set; }
        /// <summary>
        /// 返回的消息,如果code小于0则为失败信息
        /// </summary>
        public string Msg { get; set; }
        /// <summary>
        /// 返回的结果(记得修改为List<T>)
        /// </summary>
        public T Data { get; set; }
        /// <summary>
        /// 返回的结果集
        /// </summary>
        public List<T> Datas { get; set; }
        /// <summary>
        /// 扩展数据(存放一些别的数据)
        /// </summary>
        public JObject ExtendedData { get; set; }
        /// <summary>
        /// 回调函数(可指定成功/失败后执行的前端函数)
        /// </summary>
        public string RepCallBack { get; set; }

        /// <summary>
        /// 数据总条数
        /// </summary>
        [DefaultValue(0)]
        public int TotalNumber { get; set; }

        /// <summary>
        /// code<0时引发的异常信息
        /// </summary>
        [JsonIgnore]
        public Exception CurrentException { get; set; }
        /// <summary>
        /// 重写为Json
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this.ToJson();
        }
    }

    /// <summary>
    /// 封装的一个查询数据值类
    /// </summary>
    public class SearchValue
    {
        public string Name { get; set; }
        public string Value { get; set; }
        /// <summary>
        /// 只有选择项包含此列
        /// </summary>
        public List<SearchValue> SubSelected { get; set; }
    }
    /// <summary>
    /// 数据搜索
    /// </summary>
    public class SearchData
    {

        public List<SearchValue> SearchKeyData { get; set; } = new List<SearchValue>();
        public List<SearchValue> SearchNumKeyData { get; set; } = new List<SearchValue>();
        public List<SearchValue> SearchSelKeyData { get; set; } = new List<SearchValue>();
        public List<SearchValue> SearchDateSelKeyData { get; set; } = new List<SearchValue>();
    }
}
