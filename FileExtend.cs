﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Msdn5.Framework
{
    /// <summary>
    /// 一个File扩展类(只读的形式打开文件,不锁定文件)
    /// 传入路径后不用再去自己读取内容.
    /// 如果文件不存在则为null
    /// </summary>
    public class FileExtend
    {
         
        public FileExtend(string path)
        {
            if (path.NotNullOrEmpty())
            {
                Info = new FileInfo(path);
                if (Info.Exists)
                {
                    FileBuffer = new byte[Info.Length];
                    using (var fs = Info.Open(FileMode.Open, FileAccess.Read, FileShare.Read))
                    {
                        fs.Read(FileBuffer, 0, FileBuffer.Length);
                    }
                }
            }
        }
        /// <summary>
        /// 文件信息
        /// </summary>
        public FileInfo Info { get; set; }
        /// <summary>
        /// 文件内容
        /// </summary>
        public byte[] FileBuffer { get; set; }
        /// <summary>
        /// 文件类型,仅通过后缀名判断
        /// </summary>
        public FileTypeEnum FileType
        {
            get
            {
                switch (Info.Extension.Replace(".", ""))
                {

                    case "avi":
                    case "wmv":
                    case "mpeg":
                    case "mp4":
                    case "m4v":
                    case "mov":
                    case "asf":
                    case "flv":
                    case "f4v":
                    case "rmvb":
                    case "rm":
                    case "3gp":
                    case "vob":
                        return FileTypeEnum.Video;
                    case "bmp":
                    case "jpg":
                    case "png":
                    case "tif":
                    case "gif":
                    case "pcx":
                    case "tga":
                    case "exif":
                    case "fpx":
                    case "svg":
                    case "psd":
                    case "cdr":
                    case "pcd":
                    case "dxf":
                    case "ufo":
                    case "eps":
                    case "ai":
                    case "raw":
                    case "WMF":
                    case "webp":
                    case "avif":
                    case "apng":
                        return FileTypeEnum.Image;
                    default:
                        return FileTypeEnum.UnKnow;
                }
            }
        }


    }

    public enum FileTypeEnum
    {
        UnKnow,
        Video,
        Image
    }
}
